// set the background color of the master UIView
Titanium.UI.setBackgroundColor('#000');

//load the questions json
var fileName = 'questions.json';
var file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory + "json", fileName);
var preParseQuestionsData = (file.read().text);
var questions = JSON.parse(preParseQuestionsData);

// create tab group
var tabGroup = Titanium.UI.createTabGroup({
	navBarHidden : true,

});

var splashWindow = Titanium.UI.createWindow({
	title : 'Splash',
	fullscreen : false,
	exitOnClose : true,
	theme : "Theme.NoActionBar",
	navBarHidden : true,
	tabBarHidden : true,
	backgroundColor : "#e3665d"
});



var splashWindow2 = Titanium.UI.createWindow({
	title : 'Splash2',
	fullscreen : false,
	theme : "Theme.NoActionBar",
	exitOnClose : true,
	navBarHidden : true,
	tabBarHidden : true,
	backgroundColor : "#5d93e3",
	left:300
});

//splashWindow2.hideNavBar();
//splashWindow2.hideTabBar();

var tab1 = Titanium.UI.createTab({
	
	window:splashWindow
});

var tab2 = Titanium.UI.createTab({
	
	window:splashWindow2
});


var containComment = Titanium.UI.createView({
    borderRadius:10,
   backgroundColor:'red',
   width:50,
   height:50,
   left:50
});

splashWindow.add(containComment);


splashWindow.open();
//splashWindow2.open();

//
//  add tabs
//
//tabGroup.addTab(tab1);

//tabGroup.addTab(tab2);


tabGroup.addEventListener("open", function() {
	
	var actionBar = tabGroup.getActivity().actionBar;
	if (actionBar) {
		//actionBar.hide();
	}
});
// open tab group
//tabGroup.open();
