// set the background color of the master UIView
Titanium.UI.setBackgroundColor('#000');

//load the questions json
var fileName = 'questions.json';
var file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory + "json", fileName);
var preParseQuestionsData = (file.read().text);
var questions = JSON.parse(preParseQuestionsData);
var backgroundColor = "#e3665d";
var current = 0;
var currentAnswers = null;
var nextAnswers = null;
var currentAnswer = -1;
var currentAnswerLabel = null;
//animations
var inAnim = Ti.UI.createAnimation({
	left : 0,
	duration : 200
}); 

var outAnim = Ti.UI.createAnimation({
	left : -PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth),
	duration : 200
}); 

outAnim.addEventListener('complete',function(){
	
	currentView = nextView;
});



//create main Window
var mainWindow = Titanium.UI.createWindow({
	title : 'mainWindow',
	fullscreen : false,
	exitOnClose : true,
	theme : "Theme.NoActionBar",
	navBarHidden : true,
	tabBarHidden : true,
	backgroundColor : backgroundColor
});

function getAnswers(__answers){
	
	return __answers.split(",");
}



function createButton(__label){
	
	var view = Titanium.UI.createView({

			backgroundColor : "black",
			width : Ti.UI.FILL,
			height : 50,
			bottom:0
		});
		
	var label = Titanium.UI.createLabel({
			color : 'white',
			id:"label",
			text : __label,
			font : {
				fontSize : 20,
				fontFamily : 'Helvetica Neue'
			},
			textAlign : 'center',
			width : 'auto'
		}); 
		
		view.add(label);
		
		return view;
}

//create our qeustion view
function createView() {

	if (current < questions.length) {

		var cview = Titanium.UI.createView({

			backgroundColor : questions[current].background,
			width : PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth),
			height : Ti.UI.FILL,
			
		});
		
		var clabel = Titanium.UI.createLabel({
			color : 'white',
			text : questions[current].question,
			font : {
				fontSize : 20,
				fontFamily : 'Helvetica Neue'
			},
			textAlign : 'center',
			width : 'auto'
		}); 
		
		cview.add(clabel);
		var noButton = createButton("NO");
		noButton.bottom = 0;
		noButton.backgroundColor = "red";
		
		cview.add(noButton);
		var answerButton = createButton(getAnswer());
		answerButton.bottom = 50;
		answerButton.backgroundColor = "#2e640e";
		cview.add(answerButton);
		currentAnswerLabel = answerButton;
		
		noButton.addEventListener("click",noAnswer);
		answerButton.addEventListener("click",getNextView);
		
		current ++;
		return cview;
	}
}



function getNextView() {

	if (current < questions.length) {
		currentAnswer = -1;
		currentAnswers = getAnswers(questions[current].answers);
		nextView = createNextView();
		mainWindow.add(nextView);
		currentView.animate(outAnim);
		nextView.animate(inAnim);
	}
}



function getAnswer(){
	
	if(currentAnswer < currentAnswers.length-1){
		
		
		currentAnswer++;
	}else{
		
		currentAnswer = 0;
	}
	
	return currentAnswers[currentAnswer];
}

function noAnswer(e){
	
	currentAnswerLabel.children[0].text = getAnswer();	
}

//listen to or
Ti.Gesture.addEventListener('orientationchange', function(e) {

	if (e.orientation === Ti.UI.LANDSCAPE_RIGHT || e.orientation === Ti.UI.LANDSCAPE_LEFT) {

	} else {

	}
	
	if(nextView!=undefined){
		
		nextView.left = PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth);
		nextView.width = PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth);
	}
	
	currentView.width = PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth);
	currentView.left = 0;
});


function screenWidth()
{
    return (Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi)+"dp";
}

function PixelsToDPUnits(ThePixels)
{
  return (ThePixels / (Titanium.Platform.displayCaps.dpi / 160));
}

function DPUnitsToPixels(TheDPUnits)
{
  return (TheDPUnits * (Titanium.Platform.displayCaps.dpi / 160));
}

function createNextView(){
	
	var view = createView();
	view.left = PixelsToDPUnits(Titanium.Platform.displayCaps.platformWidth);
	
	return view;
}

currentAnswers = getAnswers(questions[current].answers);

var currentView = createView(getAnswer);
var nextView = null;
mainWindow.add(currentView);

mainWindow.open();


